package com.david.offload;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.david.offload.domain.AppData;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
public class FireAndForgetTest {

  @Autowired
  ObjectMapper mapper;

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void testFandF() throws Exception {
    checkFireAndForget(5);
  }

  @Test
  public void testBadResultId() throws Exception {
    mockMvc.perform(get("/result/bob"))
        .andExpect(status().isNotFound());
  }


  void checkFireAndForget(int delay) throws Exception {
    LocalDateTime start = LocalDateTime.now();
    mockMvc.perform(get("/app/fandf/" + delay))
        .andExpect(status().isAccepted())
        .andExpect(ra -> {
          LocalDateTime afterResponse = LocalDateTime.now();
          assertThat(ChronoUnit.MILLIS.between(start, afterResponse)).isLessThan(200);
          String location = ra.getResponse().getHeader(HttpHeaders.LOCATION);
          String prefix = "http://locahost:8080/";
          assertThat(location.startsWith(prefix));
          String path = location.substring(prefix.length());
          mockMvc.perform(get(path)).andExpect(status().isAccepted());
          TimeUnit.SECONDS.sleep(delay);
          MvcResult data = mockMvc.perform(get(path)).andExpect(status().isOk()).andReturn();
          String json = data.getResponse().getContentAsString();
          AppData resultData = mapper.readValue(json, AppData.class);
          assertThat(resultData.getDelaySeconds()).isEqualTo(delay);
          assertThat(resultData.getThreadName()).containsSubsequence("Wurker");
          assertThat(resultData.getStart().plusSeconds(delay)).isBefore(resultData.getEnd());
        });
  }


}
