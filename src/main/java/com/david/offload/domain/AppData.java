package com.david.offload.domain;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppData {

  private LocalDateTime start;
  private LocalDateTime end;
  private int delaySeconds;
  private String threadName;
}
