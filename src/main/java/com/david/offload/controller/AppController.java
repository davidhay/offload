package com.david.offload.controller;

import com.david.offload.domain.AppData;
import com.david.offload.domain.Time;
import com.david.offload.service.AppService;
import com.david.offload.service.ResultService;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app")
@Slf4j
public class AppController {

  @Autowired
  @Qualifier("appExecutor")
  private ExecutorService appExecutor;

  @Autowired
  private AppService appService;

  @Autowired
  private ResultService resultService;

  @RequestMapping("/time")
  public Time getTime(){
    return Time.builder().time(LocalDateTime.now()).build();
  }

  @RequestMapping(value = {"/delay", "/delay/{delay}"})
  public AppData delay(@PathVariable("delay") Optional<Integer> delayOpt) {
    int delay = delayOpt.orElse(5);
    return appService.getDelayed(delay);
  }

  @RequestMapping(value = {"/worker", "/worker/{delay}"})
  public CompletableFuture<AppData> delayOnWorker(@PathVariable("delay") Optional<Integer> delayOpt){
    int delay = delayOpt.orElse(5);
    return delayOnWorkerThread(delay);
  }

  @RequestMapping(value = {"/fandf", "/fandf/{delay}"})
  public ResponseEntity<String> fireAndForget(@PathVariable("delay") Optional<Integer> delayOpt)
      throws URISyntaxException {
    String id = UUID.randomUUID().toString();
    Consumer<AppData> callback = resultService.startRequest(id);
    int delay = delayOpt.orElse(5);
    delayOnWorkerThread(delay).thenAccept(callback::accept);
    URI location = new URI("http://localhost:8080/app/result/"+id);
    return ResponseEntity.accepted().location(location).body(id);
  }


  public CompletableFuture<AppData> delayOnWorkerThread(int delay) {
    return CompletableFuture.supplyAsync(()-> this.appService.getDelayed(delay), appExecutor);
  }

  @RequestMapping("/result/{id}")
  public ResponseEntity<AppData> result(@PathVariable("id") String id){
    Optional<AppData> resultOpt = resultService.getResult(id);
    final ResponseEntity<AppData> result;
    if (resultOpt == null) {
      result =  ResponseEntity.notFound().build();
    } else if (resultOpt.isPresent()) {
      result = ResponseEntity.ok(resultOpt.get());
    } else {
      result = ResponseEntity.accepted().build();
    }
    log.info("result for {} is {}",id,result);
    return result;
  }
}
