package com.david.offload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OffloadApplication {

	public static void main(String[] args) {
		SpringApplication.run(OffloadApplication.class, args);
	}

}
