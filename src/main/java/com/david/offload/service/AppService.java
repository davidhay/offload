package com.david.offload.service;

import com.david.offload.domain.AppData;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class AppService {


  public AppData getDelayed(int delay)  {
    LocalDateTime request = LocalDateTime.now();
    try {
      TimeUnit.SECONDS.sleep(delay);
    }catch(InterruptedException ex){
      throw new RuntimeException(ex);
    }
    LocalDateTime response = LocalDateTime.now();
    return AppData.builder().
        delaySeconds(delay).
        start(request).
        end(response).
        threadName(Thread.currentThread().getName()).
        build();
  }

}
