package com.david.offload.service;

import com.david.offload.domain.AppData;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import javax.swing.text.html.Option;
import org.springframework.stereotype.Service;

@Service
public class ResultService {

  private final Map<String, Optional<AppData>> resultstore = new HashMap<>();

  public Consumer<AppData> startRequest(String id) {
    resultstore.put(id, Optional.empty());
    return (appData) -> resultstore.put(id, Optional.of(appData));
  }

  public Optional<AppData> getResult(String id) {
    return resultstore.getOrDefault(id, null);
  }

}
