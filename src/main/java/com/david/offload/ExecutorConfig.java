package com.david.offload;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExecutorConfig {

  private ThreadFactory namedThreadFactory =
      new ThreadFactoryBuilder().setNameFormat("my-Wurker-thread-%d").build();

  @Bean
  public ExecutorService appExecutor() {
    return Executors.newFixedThreadPool(10, namedThreadFactory);
  }

}
